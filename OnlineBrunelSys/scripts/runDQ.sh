#!/bin/bash
#
export UTGID
export LOGFIFO
export PARTITION;
export NBOFSLAVES
export DIM_DNS_NODE=${4}
if test "${PARTITION}" = ""; then export PARTITION=${PARTITION_NAME}; fi;
#
cd `dirname $0`/../..
#
export CMTCONFIG=x86_64-slc6-gcc48-dbg;
. setup.${CMTCONFIG}.vars
#
export PYTHONPATH=/group/online/dataflow/options/${PARTITION}:/group/online/hlt/conditions/RunChangeHandler:${PYTHONPATH}
#
ulimit -d 2097152
ulimit -m 2097152
ulimit -v 2097152
#
if test -z "${RUNINFO}"; then
  export RUNINFO=/group/online/dataflow/options/${PARTITION}/OnlineEnvBase.py;
fi;
eval `python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py  --runinfo ${RUNINFO} --brunel --environ`;
#
#
if test "${exec_restart}" = "restart" -o "${APP_STARTUP_OPTS}" = "-restore";
then
    export LOCAL_CHECKPOINT_DIR=/scratchlocal/checkpoints;
    #
    ###python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py  --runinfo ${RUNINFO} --copy --libs --start --brunel
    eval `python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py  --runinfo ${RUNINFO} --copy --libs --start --brunel`;
    eval "python ${FARMCONFIGROOT}/job/ConfigureFromCheckpoint.py | ${RESTORE_CMD}";
else
    ## Normal running, nothing special
    echo "[INFO] +++ Starting DQWorker ${UTGID} with DNS:${DIM_DNS_NODE} Version:${BRUNELROOT}";
    export LD_PRELOAD=${CHECKPOINTING_BIN}/lib/libCheckpointing.so;
    exec -a ${UTGID} `which GaudiCheckpoint.exe` libGaudiOnline.so OnlineTask \
	-msgsvc=LHCb::FmcMessageSvc \
	-tasktype=LHCb::Class1Task \
	-main=/group/online/dataflow/templates/options/Main.opts \
	${APP_STARTUP_OPTS} -opt=command="\
import Gaudi,GaudiKernel.ProcessJobOptions;\
from Gaudi.Configuration import importOptions;\
GaudiKernel.ProcessJobOptions.printing_level=999;\
importOptions('OnlineBrunelSys/python/DQWorker.py');"
fi;
