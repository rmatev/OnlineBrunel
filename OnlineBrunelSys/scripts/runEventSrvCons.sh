#!/bin/bash
#
UTGID=Markus
LOGFIFO=/tmp/logGaudi.fifo
PARTITION=LHCb
#
#
#
export UTGID
export LOGFIFO
export PARTITION;
export DIM_DNS_NODE=${4}
if test "${PARTITION}" = ""; then export PARTITION=${PARTITION_NAME}; fi;
#
cd `dirname $0`/../..
#
export CMTCONFIG=x86_64-slc6-gcc49-do0;
. /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars
#
export PYTHONPATH=/group/online/dataflow/options/${PARTITION}:/group/online/hlt/conditions/RunChangeHandler:${PYTHONPATH}
#
if test -z "${RUNINFO}"; then
  export RUNINFO=/group/online/dataflow/options/${PARTITION}/OnlineEnvBase.py;
fi;
#
echo "[INFO] +++ Starting DQWorker ${UTGID} with DNS:${DIM_DNS_NODE} Version:${BRUNELROOT}";
export DIM_DNS_NODE=mona08;
export DATAINTERFACE=mona0801-d1;
export TAN_NODE=$DATAINTERFACE;
export TAN_PORT=YES;
export USER=online
export HOME=/home/${USER};
TASK_CLASS_TYPE=Class2;
export PARTITION=LHCb;
export ONLINETASKS=/group/online/dataflow/templates;
export PARTITIONOPTS=/group/online/dataflow/options/${PARTITION}/${PARTITION}_Info.opts;
export EVENT_SERVER=$DATAINTERFACE::LHCb_MONA0801_EventSrv2;


####/home/frankm/bin/debug2 --args `which GaudiOnlineExe.exe` libGaudiOnline.so OnlineTask \


exec -a ${UTGID} `which GaudiOnlineExe.exe` libGaudiOnline.so OnlineTask \
    -msgsvc=MessageSvc -main=$GAUDIONLINEROOT/options/Main.opts -auto \
    -tasktype=LHCb::${TASK_CLASS_TYPE}Task \
    -opt=$GAUDIONLINEROOT/options/NetworkConsumer.opts
