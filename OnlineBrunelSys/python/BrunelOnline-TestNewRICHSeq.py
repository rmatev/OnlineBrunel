"""
     Run Brunel in the online environment

     @author M.Frank
"""
__version__ = "$Id: BrunelOnline.py,v 1.25 2010/11/09 12:20:55 frankb Exp $"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
import Configurables as Configs
import Gaudi.Configuration as Gaudi
import GaudiKernel
from GaudiKernel.ProcessJobOptions import PrintOff, InstallRootLoggingHandler,logging
from Configurables import CondDB, GaudiSequencer, EventPersistencySvc, \
    HistogramPersistencySvc, EventLoopMgr, OutputStream, Gaudi__SerializeCnvSvc, \
    DstConf

#PrintOff(999)
#InstallRootLoggingHandler(level=logging.CRITICAL)

processingType ='DataTaking'

GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
requirement = None

debug = 0
def dummy(*args,**kwd): pass

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

def configureForking(appMgr):
  import OnlineEnv
  from Configurables import LHCb__CheckpointSvc
  numChildren = os.sysconf('SC_NPROCESSORS_ONLN')
  if os.environ.has_key('NBOFSLAVES'):
    numChildren = int(os.environ['NBOFSLAVES'])

  print '++++ configure Brunel for forking with ',numChildren,' children.'
  sys.stdout.flush()
  forker = LHCb__CheckpointSvc("CheckpointSvc")
  forker.NumberOfInstances   = numChildren
  forker.Partition           = OnlineEnv.PartitionName
  forker.TaskType            = os.environ['TASK_TYPE']
  forker.UseCores            = False
  forker.ChildSessions       = False
  forker.FirstChild          = 0
  # Sleep in [ms] for each child in batches of 10:
  forker.ChildSleep          = 500;
  forker.UtgidPattern        = "%P_%NN_%T_%02d"
  forker.ForceUtgid          = 1  # Important: Otheriwse tmKill does not work!
  forker.PrintLevel          = 3  # 1=MTCP_DEBUG 2=MTCP_INFO 3=MTCP_WARNING 4=MTCP_ERROR
  forker.OutputLevel         = 3  # 1=VERBOSE 2=DEBUG 3=INFO 4=WARNING 5=ERROR 6=FATAL
  appMgr.ExtSvc.insert(0,forker)
  return forker

#============================================================================================================
def patchBrunel(true_online_version):
  """
        Instantiate the options to run Brunel with raw data

        @author M.Frank
  """
  from   Configurables import CondDB, TrackSys
  import GaudiConf.DstConf
  import Brunel.Configuration
  #from Configurables import Brunel
  import ConditionsMap
  import OnlineEnv

  brunel = Brunel.Configuration.Brunel()
  #brunel = Brunel()
  
  brunel.OnlineMode = True
  try:
    brunel.DDDBtag  = OnlineEnv.DDDBTag
  except:
    print "[WARN] DDDBTag not found, use default"
  try:
    brunel.CondDBtag = OnlineEnv.CondDBTag
  except:
    print "[WARN] CondDBTag not found, use default"

  # Stops the reconstruction if tracking hits beyond some reasonable limit.
  if hasattr(OnlineEnv, 'Activity') and OnlineEnv.Activity.endswith('LEAD'):
    TrackSys.DefaultpAGlobalCuts = {'Velo': 3000, 'OT': 15000, 'IT': 999999}
    brunel.SpecialData = ['pA']

  brunel.Detectors =  ['Velo', 'PuVeto', 'Rich1', 'Rich2', 'TT', 'IT', 'OT', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr']
  brunel.DataType = "2016"
  brunel.UseDBSnapshot = True  # Try it

  conddb = CondDB()
  conddb.Online = True
  conddb.Tags['ONLINE'] = 'fake'
  print "[WARN] Running OnlineBrunel v6r1!"
  #
  # Adjust to pickup the proper online conditions from ConditionsMap
  #
  ConditionsMap.RunChangeHandlerConditions.update({
  	'Rich/rich1_readout_v3.xml' : ["Conditions/Online/Rich1/DetectorNumbers"],
        'Rich/rich2_readout_v6.xml' : ["Conditions/Online/Rich2/DetectorNumbers"]
        })
  conddb.RunChangeHandlerConditions = ConditionsMap.RunChangeHandlerConditions
  conddb.setProp('EnableRunChangeHandler', True)
  #
  # Brunel output configuration
  #
  brunel.WriteFSR  = False     # This crashes Jaap's stuff
  EventLoopMgr().OutputLevel = MSG_INFO #ERROR
  EventLoopMgr().Warnings    = False

  # from Configurables import EventClockSvc
  # EventClockSvc().InitialTime = 1478545294000000000
  
  if true_online_version:
    brunel.OutputLevel       = MSG_INFO
    brunel.PrintFreq         = -1

  from Configurables import RecMoniConf, RecSysConf

  # Forcing the rich sequence:
  richSeq = "RICHFUTURE"
  #richSeq = "RICH"
  #print "Using RICH Sequence", richSeq
  brunel.RichSequences = [richSeq]
  RecSysConf().RichSequences = [richSeq]
  RecMoniConf().RichSequences = [richSeq]
   
  # Disable HLT2 monitoring
  RecMoniConf().MoniSequence = RecMoniConf().KnownMoniSubdets
  if 'Hlt' in RecMoniConf().MoniSequence:
    RecMoniConf().MoniSequence.remove('Hlt')

  # Hack to disable old RICH monitoring
  if richSeq == "RICHFUTURE" :
    if "RICH" in RecMoniConf().MoniSequence:
      RecMoniConf().MoniSequence.remove('RICH')

  # if running old RICH, remove the new one ...
  if richSeq == "RICH" :
    if "RICHFUTURE" in RecMoniConf().MoniSequence:
      RecMoniConf().MoniSequence.remove('RICHFUTURE')    

  brunel.WriteLumi = False
  brunel.Histograms = 'Online'
  Brunel.Configuration.Brunel.configureOutput = dummy
  HistogramPersistencySvc().OutputFile = ""
  HistogramPersistencySvc().OutputLevel = MSG_ERROR

  from Gaudi.Configuration import importOptions
  importOptions("$L0TCK/L0DUConfig.opts")

  #print brunel
  #print RecSysConf()
  #print RecMoniConf()
  #print RichRecQCConf()
  return brunel

#============================================================================================================
def setupOnline():
  """
        Setup the online environment: Buffer managers, event serialisation, etc.

        @author M.Frank
  """
  import OnlineEnv

  buffs = ['Events','Output']
  app=Gaudi.ApplicationMgr()
  app.AppName = ''
  app.HistogramPersistency = 'ROOT'
  if Gaudi.allConfigurables.has_key('EventSelector'):
    del Gaudi.allConfigurables['EventSelector']
  app.SvcOptMapping.append('LHCb::OnlineEvtSelector/EventSelector')
  app.SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')
  mep = OnlineEnv.mepManager(OnlineEnv.PartitionID,OnlineEnv.PartitionName,buffs,True)
  mep.ConnectWhen = "start";
  sel = OnlineEnv.mbmSelector(input=buffs[0],type='ONE',decode=False,event_type=2)
  if requirement:
    print '++++ Warning: Setting requirements:',requirement
    sel.REQ1 = requirement
  app.EvtSel  = sel
  app.Runable = OnlineEnv.evtRunable(mep)
  app.Runable.NumErrorToStop = -1;
  app.ExtSvc.append(mep)
  app.ExtSvc.append(sel)
  app.AuditAlgorithms = False
  app.TopAlg.insert(0,"TestHistogramAlg")
  app.TopAlg.insert(0,"UpdateAndReset")
  Configs.MonitorSvc().OutputLevel = MSG_INFO
  Configs.MonitorSvc().DimUpdateInterval = 120
  Configs.MonitorSvc().CounterUpdateInterval = 15
  Configs.MonitorSvc().UniqueServiceNames = 1
  Configs.RootHistCnv__PersSvc("RootHistSvc").OutputLevel = MSG_INFO
  app.OutputLevel = MSG_INFO
  if OnlineEnv.RecoStartupMode>0:      ### os.environ.has_key('NBOFSLAVES'):
    configureForking(app)

#============================================================================================================
def patchMessages():
  """
        Messages in the online get redirected.
        Setup here the FMC message service

        @author M.Frank
  """
  import OnlineEnv
  app=Gaudi.ApplicationMgr()
  Configs.AuditorSvc().Auditors = []
  app.MessageSvcType = 'LHCb::FmcMessageSvc'
  if Gaudi.allConfigurables.has_key('MessageSvc'):
    del Gaudi.allConfigurables['MessageSvc']
  msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
  msg.fifoPath      = os.environ['LOGFIFO']
  msg.LoggerOnly    = True
  msg.doPrintAlways = False
  msg.OutputLevel   = MSG_INFO # OnlineEnv.OutputLevel

#============================================================================================================
def start():
  """
        Finish configuration and configure Gaudi application manager

        @author M.Frank
  """
  import OnlineEnv
  OnlineEnv.end_config(False)
  #OnlineEnv.end_config(True)

#============================================================================================================
def getProcessingType():
  import os
  import OnlineEnv

  if (hasattr(OnlineEnv,'ActivityType') and getattr(OnlineEnv,'ActivityType') == 'Reprocessing'):
    return 'Reprocessing'
  if os.environ.has_key('Standalone_test'):
    return 'Reprocessing'
  return 'DataTaking'

processingType = getProcessingType()
true_online = os.environ.has_key('LOGFIFO') and len(os.environ['LOGFIFO'])>0
true_online = true_online and (os.environ.has_key('PARTITION') or os.environ.has_key('PARTITION_NAME'))
debug = not true_online

if not true_online:
  print '\n            Running terminal version 1.1 of Brunel Online\n\n'
  requirement = "EvType=2;TriggerMask=0x0,0x14000,0x0,0x0;VetoMask=0,0,0,0x300;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0"

br = patchBrunel(true_online)
setupOnline()
if true_online: patchMessages()
start()
